import math

class Collision():

  def isProduced(self, bulletX, bulletY, enemyX, enemyY):
    distance = math.sqrt(math.pow(enemyX - bulletX, 2) + (math.pow(enemyY - bulletY, 2)))
    if distance < 27:
        return True