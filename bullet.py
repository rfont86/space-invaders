class Bullet():

  def __init__(self):
    self.img = 'images/bullet.png'
    self.positionY = 485
    self.bullet_speed = 0

  def fire(self):
    self.bullet_speed = -15

  def update(self):
    self.positionY += self.bullet_speed

    if self.positionY < 0:
      self.reset()

  def reset(self):
    self.positionY = 485
    self.bullet_speed = 0