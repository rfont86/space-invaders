HERO_IMAGE = 'images/astronave.png'

INITIAL_POSITION_X = 370
INITIAL_POSITION_Y = 480
STOPPED = 0
SPEED = 5

LEFT_SCREEN_LIMIT = 0
RIGHT_SCREEN_LIMIT = 736

class Player():

  def __init__(self):
    self.heroImage = HERO_IMAGE
    self.positionX = INITIAL_POSITION_X
    self.positionY = INITIAL_POSITION_Y
    self.change_position = STOPPED

  def move_right(self):
    self.change_position = SPEED

  def move_left(self):
    self.change_position = -SPEED

  def stop(self):
    self.change_position = STOPPED

  def update(self):
    self.positionX += self.change_position

    if self.positionX < LEFT_SCREEN_LIMIT:
      self.positionX = LEFT_SCREEN_LIMIT
    elif self.positionX > RIGHT_SCREEN_LIMIT:
      self.positionX = RIGHT_SCREEN_LIMIT



