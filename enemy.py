ENEMY_IMAGE = 'images/sci-fi.png'

INITIAL_POSITION_X = 370
INITIAL_POSITION_Y = 50
SPEED = 4

ADVANCE_POSITION = 20

LEFT_SCREEN_LIMIT = 0
RIGHT_SCREEN_LIMIT = 736

class Enemy():

  def __init__(self):
    self.img = 'images/sci-fi.png'
    self.positionX = INITIAL_POSITION_X
    self.positionY = INITIAL_POSITION_Y
    self.positionX_change = SPEED

  def update(self):
    self.positionX += self.positionX_change
    
    if self.positionX < LEFT_SCREEN_LIMIT:
      self.positionX_change = SPEED
      self.positionY += ADVANCE_POSITION
    elif self.positionX > RIGHT_SCREEN_LIMIT:
      self.positionX_change = -SPEED
      self.positionY += ADVANCE_POSITION