import pygame, sys

from player import Player
from enemy import Enemy
from bullet import Bullet
from collision import Collision

pygame.init()

pygame.display.set_caption('Space Invaders')
icon = pygame.image.load('images/extraterrestre.png')
pygame.display.set_icon(icon)

screen = pygame.display.set_mode((800,600))
background = pygame.image.load('images/background.png')

player = Player()
enemy = Enemy()
bullet = Bullet()
collision = Collision()

while True:
  screen.fill((0,0,0))
  screen.blit(background, (0,0))

  for event in pygame.event.get():
    if event.type == pygame.QUIT:
      sys.exit()

    if event.type == pygame.KEYDOWN:
      if event.key == pygame.K_LEFT:
        player.move_left()
      if event.key == pygame.K_RIGHT:
        player.move_right()
      if event.key == pygame.K_SPACE:
        bullet.fire()
    if event.type == pygame.KEYUP:
      if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
        player.stop()

  screen.blit(pygame.image.load(bullet.img), (player.positionX + 15, bullet.positionY))
  screen.blit(pygame.image.load(player.heroImage), (player.positionX, player.positionY))
  screen.blit(pygame.image.load(enemy.img), (enemy.positionX, enemy.positionY))

  if collision.isProduced(player.positionX, bullet.positionY, enemy.positionX, enemy.positionY):
    bullet.reset()
    enemy.positionX = 370
    enemy.positionY = 50

  bullet.update()
  player.update()
  enemy.update()

  pygame.display.update()